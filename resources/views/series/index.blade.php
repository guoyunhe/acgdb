@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @foreach ($series as $item)
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header">{{ $item->name->translations->firstWhere('locale', 'en')->content }}</div>
                    <div class="card-body">
                        {{ $item->description->trans() }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
