<?php

use App\Series;
use App\Translatable;
use App\Translation;
use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'slug' => 'yu-yu-hakusho',
                'name' => [
                    ['locale' => 'en', 'content' => 'Yu Yu Hakusho'],
                    ['locale' => 'ja', 'content' => '幽☆遊☆白書'],
                    ['locale' => 'zh', 'content' => '幽游白书'],
                ],
                'description' => [
                    [
                        'locale' => 'en',
                        'content' => <<<TEXT
Yu Yu Hakusho (Japanese: 幽☆遊☆白書 Hepburn: Yū Yū Hakusho) is a Japanese manga
series written and illustrated by Yoshihiro Togashi. The series tells the story
of Yusuke Urameshi, a teenage delinquent who is struck and killed by a car while
attempting to save a child\'s life. After a number of tests presented to him by
Koenma, the son of the ruler of the afterlife Underworld, Yusuke is revived and
appointed the title of "Underworld Detective", with which he must investigate
various cases involving demons and apparitions in the human world. The manga
becomes more focused on martial arts battles and tournaments as it progresses.
Togashi began creating Yu Yu Hakusho around November 1990, basing the series on
his interests in the occult and horror films and an influence of Buddhist
mythology.
TEXT
                    ],
                    [
                        'locale' => 'ja',
                        'content' => <<<TEXT
『幽☆遊☆白書』（ゆうゆうはくしょ）は、冨樫義博による日本の漫画作品。
TEXT
                    ],
                    [
                        'locale' => 'zh',
                        'content' => <<<TEXT
《幽游白书》（日语：幽☆遊☆白書），是日本漫画家冨㭴义博的作品，连载于《周刊少年Jump》1990
年51号到1994年32号，单行本由集英社发售，全19册。后来陆续发行电视动画、电影、完全版漫画、电
子游戏、文库版等相关作品。中文版在台湾是由东立出版社代理，香港则是由天下出版代理。
TEXT
                    ]
                ],
            ],
        ];

        foreach ($data as $item) {
            $series = new Series;
            $series->slug = $item['slug'];

            $name = Translatable::create();
            $name->translations()->createMany($item['name']);
            $series->name()->associate($name);

            $description = Translatable::create();
            $description->translations()->createMany($item['description']);
            $series->description()->associate($description);

            $series->save();
        }
    }
}
