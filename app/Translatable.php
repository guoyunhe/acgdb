<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Translatable extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'translatables';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the translations for the translatable.
     */
    public function translations()
    {
        return $this->hasMany('App\Translation');
    }

    /**
     * Get translation string of default or specific locale.
     *
     * @return string
     */
    public function trans($locale = '')
    {
        if (!$locale) {
            $locale = LaravelLocalization::getCurrentLocale();
        }

        $translation = $this->translations->firstWhere('locale', $locale);

        if ($translation) {
            return $translation->content;
        } else {
            return null;
        }
    }
}
