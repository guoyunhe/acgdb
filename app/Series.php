<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug'];

    /**
     * Get the name translatable.
     */
    public function name()
    {
        return $this->belongsTo('App\Translatable', 'name_id');
    }

    /**
     * Get the description translatable.
     */
    public function description()
    {
        return $this->belongsTo('App\Translatable', 'description_id');
    }
}
