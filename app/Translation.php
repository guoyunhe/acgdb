<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    /**
     * Get the translatable that owns the translation.
     */
    public function translatable()
    {
        return $this->belongsTo('App\Translatable');
    }
}
