#!/bin/bash

# Install openSUSE RPM packages
sudo zypper install php7 php7-openssl php7-pdo php7-mbstring php7-tokenizer \
    php7-xmlreader php7-xmlwriter php7-ctype php7-json php7-sqlite php-composer \
    nodejs npm

# composer and npm packages
composer install
npm install

# Initialize database
cp .env.example .env
touch database/database.sqlite
sed -i -e "s~<sqlite-file-path>~${PWD}/database/database.sqlite~g" .env
php artisan migrate
