# acgdb

serious acg database with open api

## development

fork this git repo and clone your fork locally.

we have a script to help you set up local development environment:

```bash
cd acgdb
./setup_devel.sh
```

start local server:

```bash
php artisan serve
```

visit http://localhost:8000/ in your web browser.

## copyright

2018 guo yunhe

## License

gnu affero general public license version 3
